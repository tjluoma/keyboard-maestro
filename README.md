
# Various macros I've made for Keyboard Maestro



## Finder Macros ##

### Show-or-Hide-All-files.kmmacros ###

Press ⌥ + H and, depending on the current state of the Finder preference, you will either see this:

![](https://bytebucket.org/tjluoma/keyboard-maestro/raw/5e3670989f175ff6f1be24de777ed07c73fbb220/finder/show-or-hide-all-files/show-or-hide-all-files-1.jpg)

or this:

![](https://bytebucket.org/tjluoma/keyboard-maestro/raw/5e3670989f175ff6f1be24de777ed07c73fbb220/finder/show-or-hide-all-files/show-or-hide-all-files-2.jpg)

